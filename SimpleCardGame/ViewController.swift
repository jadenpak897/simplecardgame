//
//  ViewController.swift
//  SimpleCardGame
//
//  Created by wjpak on 12/11/14.
//  Copyright (c) 2014 jdp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var firstCardImageView: UIImageView!
    @IBOutlet weak var secondCardImageView: UIImageView!
    @IBOutlet weak var playRoundButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var cardNamesArray:[String] = ["card_ace", "card2", "card3", "card4", "card5",
        "card6", "card7", "card8", "card9", "card10", "card_jack", "card_queen", "card_king"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.playRoundButton.setTitle("Play", forState: UIControlState.Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func playRoundTapped(sender: UIButton) {
        var firstRandomNumber = Int(arc4random_uniform(13))
        var firstCardString:String = self.cardNamesArray[firstRandomNumber]
        self.firstCardImageView.image = UIImage(named: firstCardString)
        
        var secondRandomNumber = Int(arc4random_uniform(13))
        var secondCardString:String = self.cardNamesArray[secondRandomNumber]
        self.secondCardImageView.image = UIImage(named: secondCardString)
        
        // determine the higher card
        if firstRandomNumber > secondRandomNumber {
            // todo : first card is larger
        }
        else if firstRandomNumber == secondRandomNumber {
            // todo : numbers are equal
        }
        else {
            // todo : second card is larger
        }
    }
}

